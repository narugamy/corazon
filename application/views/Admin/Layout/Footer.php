	</div>
		<footer>
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="collapse navbar-collapse">
						<p class="pull-left">Todos los derechos reservados &copy <?=date('Y')?></p>
						<p class="pull-right">ProgramoFacil</p>
					</div>
				</div>
			</nav>
		</footer>
	</div>
	<script src="<?=base_url()?>assets/librerias/Jquery/js/Jquery.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/Bootstrap/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/Chosen/chosen.jquery.js"></script>
	<script src="<?=base_url()?>assets/librerias/Trumbowyg/trumbowyg.js"></script>
	<script src="<?= base_url()?>assets/librerias/Nicescroll/js/nicescroll.min.js"></script>
	<script src="<?= base_url()?>assets/librerias/DataTables/js/jquery.dataTables.js"></script>
	<script src="<?= base_url()?>assets/librerias/DataTables/js/dataTables.bootstrap.js"></script>
	<script src="<?=base_url()?>assets/js/script.js"></script>
</body>
</html>