<!DOCTYPE html><!-- Si es HTML5 se pone esto si no no le pongas-->
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Bienvenido <?=$this->session->userdata('Nombre')?>| Panel de Administrador</title>
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/Bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/admin.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/Chosen/chosen.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/Trumbowyg/ui/trumbowyg.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/DataTables/css/dataTables.bootstrap.css">
</head>
<body>
	<div class="container" id="paneladm">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-user"></span></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav botones">
						<li><a href="<?=base_url()?>paneladmin" class="ajax">Inicio </a></li>
						<li><a href="<?=base_url()?>paneladmin/types" class="ajax">Rangos</a></li>
						<li><a href="<?=base_url()?>paneladmin/companies" class="ajax">Compañias</a></li>
						<li><a href="<?=base_url()?>paneladmin/users" class="ajax">Usuarios</a></li>
						<li><a href="<?=base_url()?>paneladmin/products" class="ajax">Productos</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="<?=base_url()?>">Pagina Principal</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"> <?=$this->session->userdata('Nombre')?></a>
							<ul class="dropdown-menu">
								<li><a href="<?= base_url();?>salir">Salir</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="contenido" id="container">