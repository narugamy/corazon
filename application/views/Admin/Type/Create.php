<?php
$form = array('class' => 'login form','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Crear nuevo Rango</h3>
		</div>
		<div class="panel-body">
			<?=form_open(base_url().'paneladmin/typecrear',$form)?>
				<div class="form-group">
					<div id="exito"></div>
				</div>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name'])?>
				</div>
				<div class="form-group">
					<div id="nom"></div>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>