
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Lista de Rangos</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<?php if(!empty($alert['alert'])){?>
				<div class="<?=$alert['alertc']?>">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p><?=$alert['alert']?></p>
				</div>
				<?php } ?>
				<a href="<?=base_url()?>paneladmin/typecreate" class="btn btn-info boton ajax" id="html">Registrar nuevo Rango</a>
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<th>ID</th>
						<th>Nombre</th>
						<th>Accion</th>
					</thead>
					<tbody>
						<?php foreach($types as $type){?>
							<tr>
								<td><?= $type->id ?></td>
								<td><?= $type->name ?></td>

								<td>
									<a href="<?=base_url()?>paneladmin/typeupdate/<?=$type->id?>" class="btn btn-warning boton ajax" id="html"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
									<a href="<?=base_url()?>paneladmin/typedelete/<?=$type->id?>" class="btn btn-danger boton json" id="json" onclick="return confirm('¿Seguro que desea Eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="text-center">
			</div>
		</div>
	</div>