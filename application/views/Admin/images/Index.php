	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Lista de Imagenes</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<div class="row">
					<?php foreach($images as $image){?>
						<div class="col-sm-4 col-xs-6">
							<div class="panel panel-default">
								<div class="panel-body">
									<img src="<?= base_url()?>assets/images/<?= $image->name?>" class="img-responsive">
								</div>
								<div class="panel-footer">
									<div class="text-center">
										<?php if($image->curso){?>
											<?=$image->curso->title?>
										<?php }
										if(!empty($image->curso->id)){?>
											<br><?=$image->user->name?>
										<?php }?>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
					<div class="col-md-12 text-center">
						<?= $pagination ?>
					</div>
				</table>
			</div>
		</div>
	</div>