<?php
$form = array('class' => 'login form','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Crear Producto</h3>
		</div>
		<div class="panel-body">
			<?=form_open(base_url().'paneladmin/productcrear',$form)?>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name'])?>
				</div>
				<div class="form-group">
					<div id="nam"></div>
				</div>
				<div class="form-group">
					<label for="description">Descripcion</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Descripcion','id'=>'description','name'=>'description'])?>
				</div>
				<div class="form-group">
					<div id="des"></div>
				</div>
				<div class="form-group">
					<label for="price">Precio</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Precio','id'=>'price','name'=>'price','type'=>'number','step'=>'0.1','min'=>'1'])?>
				</div>
				<div class="form-group">
					<div id="pri"></div>
				</div>
				<div class="form-group">
					<label for="stock">Stock</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Stock','id'=>'stock','name'=>'stock','type'=>'number','step'=>'1','min'=>'1'])?>
				</div>
				<div class="form-group">
					<div id="sto"></div>
				</div>
				<div class="form-group">
					<label for="image_id">Imagen</label>
					<?=form_input(['class'=>'form-control','placeholder'=>'Imagen','id'=>'image_id','name'=>'image_id','type'=>'file'])?>
				</div>
				<div class="form-group">
					<div id="sto"></div>
				</div>
				<div class="form-group">
					<label for="company_id">Compañias</label>
					<select name="company_id" class='form-control select-category' id="company_id">
						<option value="">Ninguno</option>
						<?php foreach($companies as $company){?>
						<option value="<?=$company->id?>"><?=$company->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>