<?php
$form = array('class' => 'login form','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Editar Producto: <?=$product->name?></h3>
		</div>
		<div class="panel-body">
			<?=form_open(base_url().'paneladmin/productsave',$form)?>
			<div class="form-group">
				<label for="description">Descripcion</label>
				<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Descripcion','id'=>'description','name'=>'description','value'=>$product->description])?>
			</div>
			<div class="form-group">
				<div id="des"></div>
			</div>
			<div class="form-group">
				<?=form_input(['class'=>'form-control','id'=>'id','name'=>'id','value'=>$product->id,'type'=>'hidden'])?>
			</div>
			<div class="form-group">
				<?=form_input(['class'=>'form-control','id'=>'name','name'=>'name','value'=>$product->name,'type'=>'hidden'])?>
			</div>
			<div class="form-group">
				<label for="price">Precio</label>
				<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Precio','id'=>'price','name'=>'price','type'=>'number','step'=>'0.1','min'=>'1','value'=>$product->price])?>
			</div>
			<div class="form-group">
				<div id="pri"></div>
			</div>
			<div class="form-group">
				<label for="stock">Stock</label>
				<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Stock','id'=>'stock','name'=>'stock','type'=>'number','step'=>'1','min'=>'1','value'=>$product->stock])?>
			</div>
			<div class="form-group">
				<div id="sto"></div>
			</div>
			<div class="form-group">
				<label for="image_id">Imagen</label>
				<?=form_input(['class'=>'form-control','placeholder'=>'Imagen','id'=>'image_id','name'=>'image_id','type'=>'file'])?>
			</div>
			<div class="form-group">
				<div id="sto"></div>
			</div>
			<div class="form-group">
				<label for="company_id">Categoria</label>
				<select name="company_id" class='form-control select-category' id="company_id">
					<?php if(!empty($product->company_id->id)):?>
						<option value="<?=$product->company_id->id?>"><?=$product->company_id->name?></option>
					<?php endif; ?>
						<option value="">Ninguno</option>
					<?php foreach($companies as $company):
						if($product->company_id->id!=$company->id):?>
							<option value="<?=$company->id?>"><?=$company->name?></option>
					<?php endif;
					endforeach;?>
				</select>
			</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>