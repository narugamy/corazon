<?php
$form = array('class' => 'login','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Editar Nivel: <?=$level->id?></h3>
		</div>
		<div class="panel-body">
			<?=form_open(base_url().'paneladmin/levelactual',$form)?>
				<div class="form-group">
						<div id="exito"></div>
				</div>
				<div class="form-group">
					<?=form_input(['class'=>'form-control','id'=>'id','name'=>'id','value'=>$level->id,'type'=>'hidden'])?>
				</div>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name','value'=>$level->name])?>
				</div>
				<div class="form-group">
					<div id="nom"></div>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>