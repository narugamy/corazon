<?php
$form = array('class' => 'login','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Crear Categoria</h3>
		</div>
		<div class="panel-body">
			<?=form_open_multipart(base_url().'paneladmin/categorycrear',$form)?>
				<div class="form-group">
					<div id="exito"></div>
				</div>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name'])?>
				</div>
				<div class="form-group">
					<div id="nom"></div>
				</div>
				<div class="form-group">
				<label for="image_id">Foto</label>
					<input type="file" name="image_id" class='form-control' placeholder='Image' id="image_id">
				</div>
				<div class="form-group">
					<div id="fil"></div>
				</div>
				<div class="form-group">
					<p>* Imagen no mayor a 2MegaBytes para evitar problemas</p>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>