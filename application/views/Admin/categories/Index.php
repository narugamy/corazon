	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Lista de Categorias</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<?php if(!empty($alert['alert'])){?>
				<div class="<?=$alert['alertc']?>">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p><?=$alert['alert']?></p>
				</div>
				<?php } ?>
				<a href="<?=base_url()?>paneladmin/categorycreate" class="btn btn-info boton" id="html">Registrar nueva Categoria</a>
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<th>ID</th>
						<th>Nombre</th>
						<th>Accion</th>
					</thead>
					<tbody>
						<?php foreach($categories as $category){?>
							<tr>
								<td><?= $category->id ?></td>
								<td><?= $category->name ?></td>

								<td>
									<a href="<?=base_url()?>paneladmin/categoryupdate/<?=$category->id?>" class="btn btn-warning boton" id="html"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
									<a href="<?=base_url()?>paneladmin/categorydelete/<?=$category->id?>" class="btn btn-danger boton" id="json" onclick="return confirm('¿Seguro que desea Eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<div class="text-center">
					<?=$pagination?>
				</div>
			</div>
		</div>
	</div>