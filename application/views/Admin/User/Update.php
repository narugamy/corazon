<?php
$form = array('class' => 'login form','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Editar Usuario: <?=$user->email?></h3>
		</div>
		<div class="panel-body">
			<?=form_open(base_url().'paneladmin/usersave',$form)?>
				<div class="form-group">
						<div id="exito"></div>
				</div>
				<div class="form-group">
					<?=form_input(['class'=>'form-control','id'=>'id','name'=>'id','value'=>$user->id,'type'=>'hidden'])?>
				</div>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name','value'=>$user->name])?>
				</div>
				<div class="form-group">
					<div id="nom"></div>
				</div>
				<div class="form-group">
					<label for="apellidos">Apellidos</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Apellidos','id'=>'apellidos','name'=>'apellidos','value'=>$user->apellidos])?>
				</div>
				<div class="form-group">
					<div id="ape"></div>
				</div>
				<div class="form-group">
					<label for="date">Nacimiento</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Fecha de Nacimiento','id'=>'date','name'=>'date','type'=>'date','value'=>$user->date])?>
				</div>
				<div class="form-group">
					<div id="nac"></div>
				</div>
				<div class="form-group">
					<label for="type_id">Rango</label>	
					<select name="type_id" class='form-control' placeholder="Seleccione un Rango" id="type_id" required="">
						<option value="<?=$user->type_id->id?>"><?=$user->type_id->name?></option>
						<option value="">Ninguno</option>
						<?php foreach($types as $type):
						if($user->type_id->id!=$type->id):?>
							<option value="<?=$type->id?>"><?=$type->name?></option>
						<?php endif;
						endforeach;?>
					</select>
				</div>
				<div class="form-group">
					<div id="ran"></div>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>