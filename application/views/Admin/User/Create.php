<?php
$form = array('class' => 'login form','id'=>'form');
?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Crear Usuario</h3>
		</div>
		<div class="panel-body">
			<?=form_open_multipart(base_url().'paneladmin/usercrear',$form)?>
				<div class="form-group">
					<div id="exito"></div>
				</div>
				<div class="form-group">
					<label for="name">Nombre</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Nombre','id'=>'name','name'=>'name'])?>
				</div>
				<div class="form-group">
					<div id="nom"></div>
				</div>
				<div class="form-group">
					<label for="apellidos">Apellidos</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Apellidos','id'=>'apellidos','name'=>'apellidos'])?>
				</div>
				<div class="form-group">
					<div id="ape"></div>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Email','id'=>'email','name'=>'email','type'=>'email'])?>
				</div>
				<div class="form-group">
					<div id="ema"></div>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Password','id'=>'password','name'=>'password','type'=>'password'])?>
				</div>
				<div class="form-group">
					<div id="pas"></div>
				</div>
				<div class="form-group">
					<label for="date">Nacimiento</label>
					<?=form_input(['class'=>'form-control','required'=>'','placeholder'=>'Fecha de Ncimiento','id'=>'date','name'=>'date','type'=>'date'])?>
				</div>
				<div class="form-group">
					<div id="nac"></div>
				</div>
				<div class="form-group">
					<label for="type_id">Rango</label>
					<select name="type_id" class='form-control' placeholder="Seleccione un Rango" id="type_id" required="">
						<option value="">Seleccione un Rango</option>
						<?php foreach($types as $type){?>
						<option value="<?=$type->id?>"><?=$type->name?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<div id="ran"></div>
				</div>
				<div class="form-group">
					<div id="niv"></div>
				</div>
				<div class="form-group">
				<label for="image_id">Foto</label>
					<input type="file" name="image_id" class='form-control' placeholder='Image' id="image_id">
				</div>
				<div class="form-group">
					<div id="fil"></div>
				</div>
				<div class="form-group">
					<p>* Imagen no mayor a 2MegaBytes para evitar problemas</p>
				</div>
				<div class="form-group">
					<button type="submit" class='btn btn-primary btn-lg1 btn-block' id="enviar">Enviar</button>
				</div>
			<?= form_close();?>
		</div>
	</div>