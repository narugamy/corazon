	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Lista de Usuarios</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<?php if(!empty($alert['alert'])){?>
				<div class="<?=$alert['alertc']?>">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p><?=$alert['alert']?></p>
				</div>
				<?php } ?>
				<a href="<?=base_url()?>paneladmin/usercreate" class="btn btn-info boton" id="html">Registrar nuevo Usuario</a>
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<th>ID</th>
						<th>Nombre</th>
						<th>Apellidos</th>
						<th>Email</th>
						<th>Fecha de Nacimiento</th>
						<th>Rango</th>
						<th>Accion</th>
					</thead>
					<tbody>
						<?php foreach($users as $user){?>
							<tr>
								<td><?= $user->id ?></td>
								<td><?= $user->name ?></td>
								<td><?= $user->apellidos ?></td>
								<td><?= $user->email ?></td>
								<td><?= $user->date ?></td>
								<?php if($user->type_id->name=='admin'){?>
									<td><span class="label label-danger"><?=$user->type_id->name?></span></td>
								<?php }elseif($user->type_id->name=='user'){?>
									<td><span class="label label-info"><?=$user->type_id->name?></span></td>
								<?php }elseif($user->type_id->name=='vip'){?>
									<td><span class="label label-warning"><?=$user->type_id->name?></span></td>
								<?php }else{?>
									<td><span class="label label-success"><?=$user->type_id->name?></span></td>
								<?php }?>

								<td>
									<a href="<?=base_url()?>paneladmin/userupdate/<?=$user->id?>" class="btn btn-warning boton ajax" id="html"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
									<a href="<?=base_url()?>paneladmin/userdelete/<?=$user->id?>" class="btn btn-danger boton json" id="json" onclick="return confirm('¿Seguro que desea Eliminarlo?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>