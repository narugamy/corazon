
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title titulo">Lista de Compañias</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<?php if(!empty($alert['alert'])){?>
				<div class="<?=$alert['alertc']?>">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<p><?=$alert['alert']?></p>
				</div>
				<?php } ?>
				<a href="<?=base_url()?>paneladmin/companycreate" class="btn btn-info boton ajax">Registrar nueva Compañia</a>
			</div>
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<th>ID</th>
						<th>Nombre</th>
						<th>Accion</th>
					</thead>
					<tbody>
						<?php foreach($companies as $company){?>
							<tr>
								<td><?= $company->id ?></td>
								<td><?= $company->name ?></td>

								<td>
									<a href="<?=base_url()?>paneladmin/companyupdate/<?=$company->id?>" class="btn btn-warning boton ajax"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a>
									<a href="<?=base_url()?>paneladmin/companydelete/<?=$company->id?>" class="btn btn-danger boton json"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="text-center">
			</div>
		</div>
	</div>