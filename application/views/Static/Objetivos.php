<div>
	<div class="row contenedor">
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Objetivos</h2>
			<p>- Llevar a cabo la atención  del cliente encaminada a asistirlo en su pedido, de manera que obtenga el mejor resultado en el tiempo óptimo y al menor costo.</p>
			<p>- Minimizar los costos de los productos en base a las ventas.</p>
			<p>- Evitar complicaciones asociadas con el uso de productos (caducidad).</p>
			<p>- Mejorar la calidad del área de ventas.</p>
		</div>
	</div>
</div>