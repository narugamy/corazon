<div>
	<div class="row contenedor">
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Vision</h2>
			<p>Ser una empresa líder, contar con precios bajos con visión de futuro, que brinde un servicio rápido y de calidad al cliente”, logrando que el cliente se fidelice e identifique con la empresa y que quede totalmente satisfecho.
				Este documento define la visión del producto desde la perspectiva del cliente, especificando las necesidades y características del producto. Constituye una base de acuerdo en cuanto a los requisitos del sistema.</p>
		</div>
		<div class="col-xs-1 circulo">
			<span class="fa fa-star"></span>
		</div>
		<div class="col-xs-9 textop">
			<h2>Mision</h2>
			<p>Brindar más salud al mejor precio ofreciendo para ello sólo medicamentos originales de procedencia garantizada así como la más alta calidad de servicio.</p>
		</div>
	</div>
</div>