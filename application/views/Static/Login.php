<div>
	<div class="row contenedor">
		<div class="login">
			<?= form_open(base_url().'sesion',['class'=>'form']) ?>
				<div class="form_group">
					<?= form_label('Email','email')?>
					<?= form_input(['type'=>'email','name'=>'email','class'=>'form-control','id'=>'email','placeholder'=>'Email','required'=>''])?>
				</div>
				<div class="form_group">
					<div id="ema"></div>
				</div>
				<div class="form_group">
					<?= form_label('Password','password')?>
					<?= form_input(['type'=>'password','name'=>'password','class'=>'form-control','id'=>'password','placeholder'=>'Password','required'=>''])?>
				</div>
				<div class="form_group">
					<div id="pas"></div>
				</div>
				<div class="form_group">
					<div id="gen"></div>
				</div>
				<div>
					<?= form_button(['class'=>'btn btn-success','content'=>'Login','type'=>'submit'])?>
				</div>
			<?= form_close() ?>
		</div>
	</div>
</div>