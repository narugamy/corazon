<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modeluser');
		$this->load->model('Modeltype');
		$this->perfil=$this->session->userdata('Perfil');
	}

	function GeneraPass(){
		$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$longitudCadena=strlen($cadena);
		$pass = "";
		$longitudPass=15;
		for($i=1 ; $i<=$longitudPass ; $i++){
			$pos=rand(0,$longitudCadena-1);
			$pass .= substr($cadena,$pos,1);
		}
		return $pass;
	}

	public function Generar(){
		if($this->session->userdata('Perfil')=='vista'){
			$this->Recupera();
			$error=array('exito'=>false,'email'=>'');
			$er=array('email'=>'');
			$email=$this->input->post('email');
			if($this->form_validation->run() == FALSE){
				$er['email']=form_error('email');
			}else{
				$dato=$this->Modeluser->get(array('email'=>$email));
				if(empty($dato->email)){
					$er['email']="El correo no existe entre los usuario";
				}else{
					$pw=$this->GeneraPass();
					$user['password']=md5($pw);
					$this->Modeluser->update($user,$dato->id);
					$this->email->from('info@programofacil.com');
					$this->email->to($dato->email);
					$this->email->subject('Recuperacion de Contraseña');
					$this->email->message("Su contraseña ha sido restablecido y se esta enviando su nueva contraseña la cual es: ".$pw);
					if($this->email->send()){
						$error['exito']=true;
						$error['alert']="Mensaje Enviado a su correo por favor revise su correo y estara su nueva contraseña";
						$error['url']=base_url()."olvideclave";
						$error['alertc']="alert alert-success alert-dismissible";
					}
				}
			}
			$error['errores']=$er;
			echo json_encode($error);
		}else{
			redirect(base_url(),'refresh');
		}
	}

	public function Index(){
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista' => 'Index'));
		}
	}

	public function Login(){
		if($this->input->is_ajax_request()) {
			if(!empty($this->perfil)){
				$this->Vista(array('vista'=>'Index','ajax'=>'ajax'));
			}else{
				$this->Vista(array('vista'=>'Login','ajax'=>'ajax'));
			}
		}else{
			if(!empty($this->perfil)){
				redirect(base_url());
			}else{
				$this->Vista(array('vista'=>'Login'));
			}
		}
	}

	public function Objetivos(){
		if($this->input->is_ajax_request()){
			$this->Vista(array('vista'=>'Objetivos','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista'=>'Objetivos'));
		}
	}

	public function Organigrama(){
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Organigrama','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista' => 'Organigrama'));
		}
	}

	function Recupera(){
		$this->form_validation->set_rules('email', 'Correo', 'trim|valid_email|min_length[15]|max_length[150]|required');
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Session(){
		if($this->input->is_ajax_request()){
			$this->Validar('');
			$er=['email'=>'','password'=>''];
			$error=['exito'=>false];
			if($this->form_validation->run() == FALSE){
				$er['email']=form_error('email');
				$er['password']=form_error('password');
			}else{
				if(empty($er['email']) && empty($er['password'])){
					$user=$this->Modeluser->get($this->input->post(null,true));
					if($user!=NULL){
						$user->type_id=$this->Modeltype->get(array('id'=>$user->type_id));
						$error['exito']=true;
						$data = array(
							'is_logued_in'	=>		TRUE,
							'id'			      =>		$user->id,
							'Perfil'		    =>		$user->type_id->name,
							'Nombre'		    =>		$user->name,
							'Apellidos'		  =>	  $user->apellidos,
							'Img'			      =>		$user->image_id
						);
						$this->session->set_userdata($data);
						$error['url']=base_url()."paneladmin";
					}else{
						$error['data']=$user;
						$error['general']="Verifique sus datos por favor";
					}
				}
			}
			$error['errores']=$er;
			echo json_encode($error);
		}
	}

	public function Salir(){
		if(!empty($this->session->userdata('Perfil'))){
			$this->session->sess_destroy();
			redirect(base_url());
		}else{
			redirect(base_url());
		}
	}

	public function Servicios(){
		if($this->input->is_ajax_request()){
			$this->Vista(array('vista' => 'Servicios','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista' => 'Servicios'));
		}
	}

	public function Vision(){
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Vision','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista' => 'Vision'));
		}
	}

	private function Validar($dato=null){
		$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
		if(!empty($dato)){
			$this->form_validation->set_rules('email', 'Correo', 'trim|required|xss_clean|is_unique[users.email]|min_length[6]');
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|min_length[6]');
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|xss_clean|min_length[6]');
			$this->form_validation->set_rules('date', 'Fecha de Nacimiento', 'trim|required|xss_clean');
			$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		}else{
			$this->form_validation->set_rules('email', 'Correo', 'trim|valid_email|min_length[15]|max_length[150]|required');
		}
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Static/'.$array['vista'],$array);
		}else{
			$this->load->view('Layout/Header');
			$this->load->view('Static/'.$array['vista'],$array);
			$this->load->view('Layout/Footer');
		}
	}
}
