<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modelcompany');
		$this->perfil=$this->session->userdata('Perfil');
		if($this->perfil=='user' || empty($this->perfil)){
			redirect(base_url());
		}
	}
	
	public function Actualizar(){
		if($this->input->is_ajax_request()){
			$company = $this->input->post(null,true);
			$company = $this->Dates($company,'update');
			$this->Validar();
			$errores = array();
			$error = ['name' => ''];
			if ($this->form_validation->run() == false){
				$error['name'] = form_error('name');
				$errores['exito'] = false;
				$errores['errores'] = $error;
			}else{
				if(empty($error['name'])){
					$this->Modelcompany->update($company,array('id'=>$company['id']));
					$errores['exito'] = true;
					$errores['alert'] = "Actualizacion exitosa de la compañia numero: " . $company['id'] . "";
					$errores['url'] = base_url() . "paneladmin/companies";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			echo json_encode($errores);
		}
	}
	
	public function Companies($array=null){
		$companies=$this->Modelcompany->get($array);
		foreach ($companies as $company):
			if(isset($company->image_id))
				$company->image_id=$this->Modelimage->get(array('id'=>$company->image_id));
		endforeach;
		return $companies;
	}

	public function Create(){
		if($this->input->is_ajax_request()){
			$this->Vista(array('ajax'=>'ajax','vista'=>'Create'));
		}else{
			$this->Vista(array('vista'=>'Create'));
		}
	}

	public function Crear(){
		if($this->input->is_ajax_request()){
			$errores=array();
			$this->Validar();
			$error=['name'=>''];
			$company=$this->input->post(null,true);
			if($this->form_validation->run() == false){
				$error['name']=form_error('name');
				$errores['exito']=false;
			}else {
				if (empty($error['name'])) {
					$company = $this->Dates($company,'create');
					$this->Modelcompany->insert($company);
					$errores['exito'] = true;
					$errores['alert'] = "Registro exitoso de la compañia: " . $company['name'] . "";
					$errores['url'] = base_url() . "paneladmin/companies";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			$errores['errores']=$error;
			echo json_encode($errores);
		}else{
			$errores['url'] = base_url();
		}
	}

	public function Dates($array,$accion){
		if($accion!="update"){
			$array['created_at']=date('Y-m-d H:i:s');
		}
		$array['updated_at']=date('Y-m-d H:i:s');
		return $array;
	}

	public function Delete($id=null){
		if($this->input->is_ajax_request()){
			$errores=array();
				if(!is_numeric($id)){
					$this->load->view("errores/html/error_404");
				}else{
					$company=$this->Companies(array('id'=>$id));
					if(empty($company->name)){
						$errores['url']=base_url()."paneladmin/companies";
					}else{
						$this->Modelcompany->delete(array('id'=>$id));
						$errores['exito']=true;
						$errores['alert']="Eliminacion exitosa de la compañia: ".$company->id."";
						$errores['url']=base_url()."paneladmin/companies";
						$errores['alertc']="alert alert-danger alert-dismissible";
					}
				}
			echo json_encode($errores);
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	public function Index(){
		$companies=$this->Companies();
		$aler=$this->input->post(null,true);
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax','companies' => $companies,'alert'=>$aler));
		}else{
			$this->Vista(array('vista' => 'Index', 'companies' => $companies,'alert'=>$aler));
		}
	}

	public function Update($id=null){
		if($this->input->is_ajax_request()){
			if (!is_numeric($id)) {
				$this->load->view("errores/html/error_404");
			}else{
				$company=$this->Companies(array('id'=>$id));
				if(empty($company->name)){
					$this->load->view("errores/html/error_404");
				}else {
					$array = ['vista' => 'Update', 'ajax' => 'ajax', 'company' => $company, 'title' => "Panel de Administrador editar usuario: $company->id"];
					$this->Vista($array);
				}
			}
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	private function Validar(){
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|min_length[5]|is_unique[companies.name]');
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('is_unique', 'El  nombre %s ya esta en uso');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Admin/Company/'.$array['vista'],$array);
		}else{
			$this->load->view('Admin/Layout/Header');
			$this->load->view('Admin/Company/'.$array['vista'],$array);
			$this->load->view('Admin/Layout/Footer');
		}
	}
}
