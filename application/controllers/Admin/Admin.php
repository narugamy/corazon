<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modeluser');
		$this->load->model('Modeltype');
		$this->perfil=$this->session->userdata('Perfil');
		if($this->perfil=='user' || empty($this->perfil)){
			redirect(base_url());
		}
	}

	public function Index(){
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax'));
		}else{
			$this->Vista(array('vista' => 'Index'));
		}
	}

	private function Validar($dato=null){
		$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|max_length[15]|required');
		if(!empty($dato)){
			$this->form_validation->set_rules('email', 'Correo', 'trim|required|xss_clean|is_unique[users.email]|min_length[6]');
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|min_length[6]');
			$this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|xss_clean|min_length[6]');
			$this->form_validation->set_rules('date', 'Fecha de Nacimiento', 'trim|required|xss_clean');
			$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		}else{
			$this->form_validation->set_rules('email', 'Correo', 'trim|valid_email|min_length[15]|max_length[150]|required');
		}
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Admin/'.$array['vista'],$array);
		}else{
			$this->load->view('Admin/Layout/Header');
			$this->load->view('Admin/'.$array['vista'],$array);
			$this->load->view('Admin/Layout/Footer');
		}
	}
}
