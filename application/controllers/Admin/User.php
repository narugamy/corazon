<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modeluser');
		$this->load->model('Modeltype');
		$this->perfil=$this->session->userdata('Perfil');
		if($this->perfil=='user' || empty($this->perfil)){
			redirect(base_url());
		}
	}

	public function Actualizar(){
		if($this->input->is_ajax_request()){
			$user = $this->input->post(null,true);
			$user = $this->Dates($user,'update');
			$this->Validar();
			$errores = array();
			$name=$user['name'];
			unset($user['name']);
			$error=['name'=>'','apellidos'=>'','password'=>'','date'=>'','type_id'=>''];
			if(empty($user['image_id']))
				$user['image_id']=null;
			if ($this->form_validation->run() == false){
				$error['name']=form_error('name');
				$error['apellidos']=form_error('apellidos');
				$error['password']=form_error('password');
				$error['date']=form_error('date');
				$error['type_id']=form_error('type_id');
				$errores['exito'] = false;
				$errores['errores'] = $error;
			}else{
				if(empty($error['name']) && empty($error['apellidos']) && empty($error['password']) && empty($error['date']) && empty($error['type_id'])){
					$this->Modeluser->update($user,array('id'=>$user['id']));
					$errores['exito'] = true;
					$errores['alert'] = "Actualizacion exitosa del usuario: " . $name . "";
					$errores['url'] = base_url() . "paneladmin/users";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			echo json_encode($errores);
		}
	}

	public function Create(){
		$types=$this->Modeltype->get();
		if($this->input->is_ajax_request()){
			$this->Vista(array('ajax'=>'ajax','vista'=>'Create','types'=>$types));
		}else{
			$this->Vista(array('vista'=>'Create','types'=>$types));
		}
	}

	public function Crear(){
		if($this->input->is_ajax_request()){
			$errores=array();
			$this->Validar();
			$error=['name'=>'','apellidos'=>'','email'=>'','password'=>'','date'=>'','type_id'=>''];
			$user=$this->input->post(null,true);
			if(empty($user['image_id']))
				$user['image_id']=null;
			if($this->form_validation->run() == false){
				$error['name']=form_error('name');
				$error['apellidos']=form_error('apellidos');
				$error['email']=form_error('email');
				$error['password']=form_error('password');
				$error['date']=form_error('date');
				$error['type_id']=form_error('type_id');
				$errores['exito']=false;
			}else{
				if(empty($error['name']) && empty($error['apellidos']) && empty($error['email']) && empty($error['password']) && empty($error['date']) && empty($error['type_id'])){
					$user = $this->Dates($user,'create');
					$user['password']=md5($user['password']);
					$this->Modeluser->insert($user);
					$errores['exito'] = true;
					$errores['alert'] = "Registro exitoso del usuario: " . $user['name'] . "";
					$errores['url'] = base_url() . "paneladmin/users";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			$errores['errores']=$error;
			echo json_encode($errores);
		}else{
			$errores['url'] = base_url();
		}
	}

	public function Dates($array,$accion){
		if($accion!="update"){
			$array['created_at']=date('Y-m-d H:i:s');
		}
		$array['updated_at']=date('Y-m-d H:i:s');
		return $array;
	}

	public function Delete($id=null){
		if($this->input->is_ajax_request()){
			$errores=array();
			if(!is_numeric($id)){
				$this->load->view("errores/html/error_404");
			}else{
				$user=$this->Users(array('id'=>$id));
				if(empty($user->name)){
					$errores['url']=base_url()."paneladmin/users";
				}else{
					$this->Modeluser->delete(array('id'=>$id));
					$errores['exito']=true;
					$errores['alert']="Eliminacion exitosa del usuario: ".$user->id."";
					$errores['url']=base_url()."paneladmin/users";
					$errores['alertc']="alert alert-danger alert-dismissible";
				}
			}
			echo json_encode($errores);
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	public function Index(){
		$users=$this->Users();
		$aler=$this->input->post(null,true);
		if($this->input->is_ajax_request()){
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax','users' =>$users,'alert'=>$aler));
		}else{
			$this->Vista(array('vista' => 'Index', 'users' =>$users,'alert'=>$aler));
		}
	}

	public function Users($array=null){
		$users=$this->Modeluser->get($array);
			if($array==null):
				foreach ($users as $user):
					if(!empty($user->image_id))
						$user->image_id=$this->Modelimage->get(array('id'=>$user->image_id));
					if(!empty($user->type_id))
						$user->type_id=$this->Modeltype->get(array('id'=>$user->type_id));
				endforeach;
			else:
				if(!empty($users->image_id))
					$users->image_id=$this->Modelimage->get(array('id'=>$users->image_id));
				if(!empty($users->type_id))
					$users->type_id=$this->Modeltype->get(array('id'=>$users->type_id));
			endif;
		return $users;
	}

	public function Update($id=null){
		if($this->input->is_ajax_request()){
			if (!is_numeric($id)) {
				$this->load->view("errores/html/error_404");
			}else{
				$user = $this->Users(array('id' => $id));
				if (empty($user->name)) {
					$this->load->view("errores/html/error_404");
				}else{
					$array = ['vista' => 'Update','ajax'=>'ajax', 'user' => $user,'types' => $this->Modeltype->get(),'title' => "Panel de Administrador editar usuario: $user->email"];
					$this->Vista($array);
				}
			}
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	private function Validar($dato=null){
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|min_length[6]');
		$this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|xss_clean|min_length[10]');
		$this->form_validation->set_rules('type_id', 'type_id', 'trim|required|xss_clean|is_natural_no_zero|min_length[1]');
		if(!empty($dato)){
			$this->form_validation->set_rules('password', 'Contraseña', 'trim|required|min_length[1]|max_length[15]');
			$this->form_validation->set_rules('email', 'Correo', 'trim|required|xss_clean|is_unique[user.email]|min_length[15]');
			$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		}
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Admin/User/'.$array['vista'],$array);
		}else{
			$this->load->view('Admin/Layout/Header');
			$this->load->view('Admin/User/'.$array['vista'],$array);
			$this->load->view('Admin/Layout/Footer');
		}
	}
}
