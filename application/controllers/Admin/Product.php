<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modelproduct');
		$this->load->model('Modelcompany');
		$this->perfil=$this->session->userdata('Perfil');
		if($this->perfil=='user' || empty($this->perfil)){
			redirect(base_url());
		}
	}

	public function Actualizar(){
		if($this->input->is_ajax_request()){
			$product = $this->input->post(null,true);
			$product = $this->Dates($product,'update');
			$this->Validar();
			$errores = array();
			$name=$product['name'];
			unset($product['name']);
			$error=['description'=>'','price'=>'','stock'=>''];
			if(empty($product['image_id']))
				$product['image_id']=null;
			if(empty($product['company_id']))
				$product['company_id']=null;
			if ($this->form_validation->run() == false){
				$error['description']=form_error('description');
				$error['price']=form_error('price');
				$error['stock']=form_error('stock');
				$errores['exito'] = false;
				$errores['errores'] = $error;
			}else{
				if(empty($error['description']) && empty($error['price']) && empty($error['stock'])){
					$this->Modelproduct->update($product,array('id'=>$product['id']));
					$errores['exito'] = true;
					$errores['alert'] = "Actualizacion exitosa del producto: " . $name . "";
					$errores['url'] = base_url() . "paneladmin/products";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			echo json_encode($errores);
		}
	}

	public function Create(){
		$companies=$this->Modelcompany->get();
		if($this->input->is_ajax_request()){
			$this->Vista(array('ajax'=>'ajax','vista'=>'Create','companies'=>$companies));
		}else{
			$this->Vista(array('vista'=>'Create','companies'=>$companies));
		}
	}

	public function Crear(){
		if($this->input->is_ajax_request()){
			$errores=array();
			$this->Validar();
			$error=['name'=>'','description'=>'','price'=>'','stock'=>''];
			$product=$this->input->post(null,true);
			if(empty($product['image_id']))
				$product['image_id']=null;
			if(empty($product['company_id']))
				$product['company_id']=null;
			if($this->form_validation->run() == false){
				$error['name']=form_error('name');
				$error['description']=form_error('description');
				$error['price']=form_error('price');
				$error['stock']=form_error('stock');
				$errores['exito']=false;
			}else{
				if(empty($error['name']) && empty($error['description']) && empty($error['price']) && empty($error['stock'])){
					$product = $this->Dates($product,'create');
					$this->Modelproduct->insert($product);
					$errores['exito'] = true;
					$errores['alert'] = "Registro exitoso del producto: " . $product['name'] . "";
					$errores['url'] = base_url() . "paneladmin/products";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			$errores['errores']=$error;
			echo json_encode($errores);
		}else{
			$errores['url'] = base_url();
		}
	}

	public function Dates($array,$accion){
		if($accion!="update"){
			$array['created_at']=date('Y-m-d H:i:s');
		}
		$array['updated_at']=date('Y-m-d H:i:s');
		return $array;
	}

	public function Delete($id=null){
		if($this->input->is_ajax_request()){
			$errores=array();
			if(!is_numeric($id)){
				$this->load->view("errores/html/error_404");
			}else{
				$product=$this->Products(array('id'=>$id));
				if(empty($product->name)){
					$errores['url']=base_url()."paneladmin/companies";
				}else{
					$this->Modelproduct->delete(array('id'=>$id));
					$errores['exito']=true;
					$errores['alert']="Eliminacion exitosa del producto: ".$product->id."";
					$errores['url']=base_url()."paneladmin/products";
					$errores['alertc']="alert alert-danger alert-dismissible";
				}
			}
			echo json_encode($errores);
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	public function Index(){
		$products=$this->Products();
		$aler=$this->input->post(null,true);
		if($this->input->is_ajax_request()){
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax','products' =>$products,'alert'=>$aler));
		}else{
			$this->Vista(array('vista' => 'Index', 'products' =>$products,'alert'=>$aler));
		}
	}

	public function Products($array=null){
		$products=$this->Modelproduct->get($array);
			if($array==null):
				foreach ($products as $product):
					if(!empty($product->image_id))
						$product->image_id=$this->Modelimage->get(array('id'=>$product->image_id));
					if(!empty($product->company_id))
						$product->company_id=$this->Modelcompany->get(array('id'=>$product->company_id));
				endforeach;
			else:
				if(!empty($products->image_id))
					$products->image_id=$this->Modelimage->get(array('id'=>$products->image_id));
				if(!empty($products->company_id))
					$products->company_id=$this->Modelcompany->get(array('id'=>$products->company_id));
			endif;
		return $products;
	}

	public function Update($id=null){
		if($this->input->is_ajax_request()){
			if (!is_numeric($id)) {
				$this->load->view("errores/html/error_404");
			}else{
				$product = $this->Products(array('id' => $id));
				if (empty($product->name)) {
					$this->load->view("errores/html/error_404");
				}else{
					$array = ['vista' => 'Update','ajax'=>'ajax', 'product' => $product, 'companies' => $this->Modelcompany->get(),'title' => "Panel de Administrador editar producto: $product->name"];
					$this->Vista($array);
				}
			}
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	private function Validar($dato=null){
		$this->form_validation->set_rules('description', 'Descripcion', 'trim|required|xss_clean|min_length[15]');
		$this->form_validation->set_rules('price', 'Precio', 'trim|required|xss_clean|decimal|min_length[3]');
		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|xss_clean|is_natural_no_zero|min_length[1]');
		if(!empty($dato)){
			$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|is_unique[products.name]|min_length[6]');
			$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		}
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Admin/Product/'.$array['vista'],$array);
		}else{
			$this->load->view('Admin/Layout/Header');
			$this->load->view('Admin/Product/'.$array['vista'],$array);
			$this->load->view('Admin/Layout/Footer');
		}
	}
}
