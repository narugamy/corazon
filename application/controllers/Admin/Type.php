<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type extends CI_Controller {

	private $perfil;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Modeltype');
		$this->perfil=$this->session->userdata('Perfil');
		if($this->perfil=='user' || empty($this->perfil)){
			redirect(base_url());
		}
	}
	
	public function Actualizar(){
		if($this->input->is_ajax_request()){
			$type = $this->input->post(null,true);
			$type = $this->Dates($type,'update');
			$this->Validar();
			$errores = array();
			$error = ['name' => ''];
			if ($this->form_validation->run() == false){
				$error['name'] = form_error('name');
				$errores['exito'] = false;
				$errores['errores'] = $error;
			}else{
				if(empty($error['name'])){
					$this->Modeltype->update($type,array('id'=>$type['id']));
					$errores['exito'] = true;
					$errores['alert'] = "Actualizacion exitosa del nivel: " . $type['id'] . "";
					$errores['url'] = base_url() . "paneladmin/types";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			echo json_encode($errores);
		}
	}

	public function Create(){
		if($this->input->is_ajax_request()){
			$this->Vista(array('ajax'=>'ajax','vista'=>'Create'));
		}else{
			$this->Vista(array('vista'=>'Create'));
		}
	}

	public function Crear(){
		if($this->input->is_ajax_request()){
			$errores=array();
			$this->Validar();
			$error=['name'=>''];
			$type=$this->input->post(null,true);
			if($this->form_validation->run() == false){
				$error['name']=form_error('name');
				$errores['exito']=false;
			}else {
				if (empty($error['name'])) {
					$type = $this->Dates($type,'create');
					$this->Modeltype->insert($type);
					$errores['exito'] = true;
					$errores['alert'] = "Registro exitoso del nivel: " . $type['name'] . "";
					$errores['url'] = base_url() . "paneladmin/types";
					$errores['alertc'] = "alert alert-success alert-dismissible";
				}
			}
			$errores['errores']=$error;
			echo json_encode($errores);
		}else{
			$errores['url'] = base_url();
		}
	}

	public function Dates($array,$accion){
		if($accion!="update"){
			$array['created_at']=date('Y-m-d H:i:s');
		}
		$array['updated_at']=date('Y-m-d H:i:s');
		return $array;
	}

	public function Delete($id=null){
		if($this->input->is_ajax_request()){
			$errores=array();
				if(!is_numeric($id)){
					$this->load->view("errores/html/error_404");
				}else{
					$type=$this->Types(array('id'=>$id));
					if(empty($type->name)){
						$errores['url']=base_url()."paneladmin/companies";
					}else{
						$this->Modeltype->delete(array('id'=>$id));
						$errores['exito']=true;
						$errores['alert']="Eliminacion exitosa del nivel: ".$type->name."";
						$errores['url']=base_url()."paneladmin/types";
						$errores['alertc']="alert alert-danger alert-dismissible";
					}
				}
			echo json_encode($errores);
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	public function Index(){
		$types=$this->Types();
		$aler=$this->input->post(null,true);
		if($this->input->is_ajax_request()) {
			$this->Vista(array('vista' => 'Index','ajax'=>'ajax','types' => $types,'alert'=>$aler));
		}else{
			$this->Vista(array('vista' => 'Index', 'types' => $types,'alert'=>$aler));
		}
	}

	public function Types($array=null){
		$types=$this->Modeltype->get($array);
		return $types;
	}

	public function Update($id=null){
		if($this->input->is_ajax_request()){
			if (!is_numeric($id)) {
				$this->load->view("errores/html/error_404");
			}else{
				$type=$this->Types(array('id'=>$id));
				if(empty($type->name)){
					$this->load->view("errores/html/error_404");
				}else {
					$array = ['vista' => 'Update', 'ajax' => 'ajax', 'type' => $type, 'title' => "Panel de Administrador editar nivel: $type->id"];
					$this->Vista($array);
				}
			}
		}else{
			$this->load->view("errores/html/error_404");
		}
	}

	private function Validar(){
		$this->form_validation->set_rules('name', 'Nombre', 'trim|required|xss_clean|min_length[3]|is_unique[types.name]');
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('is_unique', 'El  nombre %s ya esta en uso');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de %s caracteres permitidos');
		$this->form_validation->set_message('min_length', 'El  %s acepta como minimo %s caracteres');
	}

	public function Vista($array){
		if(!empty($array['ajax'])){
			$this->load->view('Admin/Type/'.$array['vista'],$array);
		}else{
			$this->load->view('Admin/Layout/Header');
			$this->load->view('Admin/Type/'.$array['vista'],$array);
			$this->load->view('Admin/Layout/Footer');
		}
	}
}
