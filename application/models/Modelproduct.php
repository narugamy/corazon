<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelproduct extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get($array=null){
		if(!empty($array)){
			$this->db->where($array);
			$query = $this->db->get('products');
			return $query->row();
		}else{
			$query = $this->db->get('products');
			return $query->result();
		}
	}

	function insert($array){
		$this->db->insert('products', $array);
		return $this->db->insert_id();
	}

	function update($array,$id){
		$this->db->where($id);
		return $this->db->update('products',$array);
	}

	function delete($id){
		$this->db->where($id);
		return $this->db->delete('products');
	}
}