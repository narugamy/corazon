<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modeltype extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get($array=null){
		if(!empty($array)){
			$this->db->where($array);
			$query = $this->db->get('types');
			return $query->row();
		}else{
			$query = $this->db->get('types');
			return $query->result();
		}
	}

	function insert($array){
		$this->db->insert('types', $array);
		return $this->db->insert_id();
	}

	function update($array,$id){
		$this->db->where($id);
		return $this->db->update('types',$array);
	}

	function delete($id){
		$this->db->where($id);
		return $this->db->delete('types');
	}
}