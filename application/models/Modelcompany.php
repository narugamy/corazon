<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modelcompany extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	function get($array=null){
		if(!empty($array)){
			$this->db->where($array);
			$query = $this->db->get('companies');
			return $query->row();
		}else{
			$query = $this->db->get('companies');
			return $query->result();
		}
	}

	function insert($array){
		$this->db->insert('companies', $array);
		return $this->db->insert_id();
	}

	function update($array,$id){
		unset($array['id']);
		$this->db->where($id);
		return $this->db->update('companies',$array);
	}

	function delete($id){
		$this->db->where($id);
		return $this->db->delete('companies');
	}
}