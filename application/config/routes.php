<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['vision'] = 'Home/Vision';
$route['objetivos'] = 'Home/Objetivos';
$route['organigrama'] = 'Home/Organigrama';
$route['servicios'] = 'Home/Servicios';
$route['login'] = 'Home/Login';
$route['sesion'] = 'Home/Session';
$route['salir'] = 'Home/Salir';

/*  Panel Admin  */
$route['paneladmin']='Admin/Admin/Index';

/*  Panel Users */
$route['paneladmin/users'] = 'Admin/User/Index';
$route['paneladmin/usercreate'] = 'Admin/User/Create';
$route['paneladmin/usercrear'] = 'Admin/User/Crear';
$route['paneladmin/userupdate/(:any)'] = 'Admin/User/Update/$1';
$route['paneladmin/usersave'] = 'Admin/User/Actualizar';
$route['paneladmin/userdelete/(:any)'] = 'Admin/User/Delete/$1';

/* Panel Niveles  */
$route['paneladmin/types'] = 'Admin/Type/Index';
$route['paneladmin/typecreate'] = 'Admin/Type/Create';
$route['paneladmin/typecrear'] = 'Admin/Type/Crear';
$route['paneladmin/typeupdate/(:any)'] = 'Admin/Type/Update/$1';
$route['paneladmin/typesave'] = 'Admin/Type/Actualizar';
$route['paneladmin/typedelete/(:any)'] = 'Admin/Type/Delete/$1';

/* Panel Products  */
$route['paneladmin/products'] = 'Admin/Product/Index';
$route['paneladmin/productcreate'] = 'Admin/Product/Create';
$route['paneladmin/productcrear'] = 'Admin/Product/Crear';
$route['paneladmin/productupdate/(:any)'] = 'Admin/Product/Update/$1';
$route['paneladmin/productsave'] = 'Admin/Product/Actualizar';
$route['paneladmin/productdelete/(:any)'] = 'Admin/Product/Delete/$1';

/* Panel Companias  */
$route['paneladmin/companies'] = 'Admin/Company/Index';
$route['paneladmin/companycreate'] = 'Admin/Company/Create';
$route['paneladmin/companycrear'] = 'Admin/Company/Crear';
$route['paneladmin/companyupdate/(:any)'] = 'Admin/Company/Update/$1';
$route['paneladmin/companysave'] = 'Admin/Company/Actualizar';
$route['paneladmin/companydelete/(:any)'] = 'Admin/Company/Delete/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
