$(document).on('ready', function () {

	var base_url = window.location.origin;
	Eventajax('.container', 'submit', '.form', 'json', 'action');
	Eventajax('.menus', 'click', '#menu-left li .ajax', 'html', 'href');
	Eventajax('.container', 'click', '.ajax', 'html', 'href');
	Eventajax('.container', 'click', '.json', 'json', 'href');
	Eventajax('#wrapper', 'click', '.contenido .boton2', 'html', 'href');
	Eventajax('#wrapper', 'click', '.contenido .banner a', 'html', 'href');
	Eventajax('#wrapper', 'click', '.menu-left li a', 'html', 'href');
	Eventajax('#wrapper', 'click', '.contenido .text a', 'html', 'href');

	initControls();

	function Eventajax(contenedor, evento, disparo, type, tipo) {
		$(contenedor).on(evento, disparo, function (event) {
			var web = $(this).attr(tipo);
			var datos;
			if (tipo != 'action') {
				datos = null;
			} else {
				datos = $(this).serialize();
			}
			if(type!='json'){
				$('.contenido').html('').addClass('loader');
			}
			event.preventDefault();
			fnc(web, datos, type);
		});
	}

	function fnc(web, datos, type) {
		$.post(web, datos, null, type)
			.done(function (dato) {
				if (type == 'json') {
					if (dato.exito) {
						if (dato.url != base_url + "/Corazon/paneladmin" && dato.url != base_url + "/Corazon/salir" && dato.url != base_url) {
							fnc(dato.url, {alert: dato.alert, alertc: dato.alertc}, 'html');
							window.history.pushState(null, "", dato.url);
						}else{
							window.location.href = dato.url;
						}
					} else {
						validaciones(dato);
					}
				} else {
					$('.contenido').fadeIn(10000,function () {
						$(this).html(dato).removeClass('loader');
						if($(".table").length > 0){
							tabla();
						}
					});
					window.history.pushState(null, "", web);
					initControls();
				}
			})
			.error(function () {
				alert("Error en el proceso");
			});
	}

	function initControls() {
		window.location.hash = "red";
		window.location.hash = "Red"; //chrome
		window.onhashchange = function () {
			window.location.hash = "red";
		};
	}

	function validar(id, valor, alert) {
		if (valor) {
			$('#' + id).html(valor).addClass(alert);
		} else {
			$('#' + id).text("").removeClass();
		}
	}

	function validaciones(dato) {
		if (typeof(dato.errores.name) != 'undefined') {
			validar('nam', dato.errores.name, "alert alert-danger");
		}
		if (typeof(dato.errores.ruc) != 'undefined') {
			validar('ruc', dato.errores.ruc, "alert alert-danger");
		}
		if (typeof(dato.errores.social) != 'undefined') {
			validar('soc', dato.errores.social, "alert alert-danger");
		}
		if (typeof(dato.errores.person) != 'undefined') {
			validar('per', dato.errores.person, "alert alert-danger");
		}
		if (typeof(dato.errores.address) != 'undefined') {
			validar('add', dato.errores.address, "alert alert-danger");
		}
		if (typeof(dato.errores.cellphone) != 'undefined') {
			validar('cell', dato.errores.cellphone, "alert alert-danger");
		}
		if (typeof(dato.errores.city) != 'undefined') {
			validar('city', dato.errores.city, "alert alert-danger");
		}
		if (typeof(dato.errores.dni) != 'undefined') {
			validar('dni', dato.errores.dni, "alert alert-danger");
		}
		if (typeof(dato.errores.full_name) != 'undefined') {
			validar('full', dato.errores.full_name, "alert alert-danger");
		}
		if (typeof(dato.errores.username) != 'undefined') {
			validar('user', dato.errores.username, "alert alert-danger");
		}
		if (typeof(dato.errores.phone) != 'undefined') {
			validar('pho', dato.errores.phone, "alert alert-danger");
		}
		if (typeof(dato.errores.message) != 'undefined') {
			validar('text', dato.errores.message, "alert alert-danger");
		}
		if (typeof(dato.errores.apellidos) != 'undefined') {
			validar('ape', dato.errores.apellidos, "alert alert-danger");
		}
		if (typeof(dato.errores.email) != 'undefined') {
			validar('ema', dato.errores.email, "alert alert-danger");
		}
		if (typeof(dato.errores.password) != 'undefined') {
			validar('pas', dato.errores.password, "alert alert-danger");
		}
		if (typeof(dato.errores.nacimiento) != 'undefined') {
			validar('dat', dato.errores.date, "alert alert-danger");
		}
		if (typeof(dato.errores.image_id) != 'undefined') {
			validar('fil', dato.errores.image_id, "alert alert-danger");
		}
		if (typeof(dato.general) != 'undefined') {
			validar('gen', dato.general, "alert alert-danger");
		}
	}

	function tabla() {
		$('.table').dataTable({
			"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
			"language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_",
				"sInfoEmpty":      "Mostrando registros del 0 al 0",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar: ",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			}
		});
	}

	if($(".table").length > 0){
		tabla();
	}

});